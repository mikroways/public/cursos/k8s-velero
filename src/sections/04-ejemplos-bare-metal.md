<!-- .slide: data-background="./static/bare-metal.png" data-state="custom-background-opacity" -->
# Ejemplos en bare metal<!-- .element: style="color: white" -->
----
# En bare metal todo es un poco _diferente_
----
<!-- .slide: class="list medium" -->
## Alternativas

* Proveedores soportados
  * VMWare Vsphere
  * [Portworx](https://portworx.com/)
  * [OpenEBS](https://openebs.io/)
  * [_Más opciones en este enlace_](https://velero.io/docs/v1.4/supported-providers/)
* Si no hay un proveedor entonces se una alternativa basada en [restic](https://restic.net/).
* A partir de kubernetes 1.17 y [velero 1.4 es posible utilizar CSI snapshots](https://velero.io/blog/velero-1.4-community-wave/).
* Solución incluso para trabajar en cloud con volúmenes RWX tipo NFS (EFS).
----
## Ejemplo con restic

* restic deduplica datos y almacena en S3.
* Los datos de volúmenes se almacenan junto con los objetos de k8s.
* Posee ciertas [limitaciones](https://velero.io/docs/v1.4/restic/#limitations)
* Corre como **DaemonSet** y hace uso de [_Mount propagation_](https://kubernetes.io/docs/concepts/storage/volumes/#mount-propagation)
  * Requiere kubernetes >= 1.10
----
## Instalamos una aplicación


```bash
cd ejemplos/on-prem/demo-nextcloud \
  && helmfile sync
```
En el namespace `demo-nextcloud` se instala nextcloud usando helm

<small class="fragment">
Este paso ya lo hicimos para ganar tiempo
</small>
----

## Creamos un backup

```bash
velero create backup nextcloud-on-prem \
  --include-namespaces demo-nextcloud
```

----

## Verificamos el estado

```bash
velero describe backup nextcloud-on-prem
# Para ver más detalle
velero describe backup nextcloud-on-prem --details
```
> Puede verse el progreso de restic
----

## Eliminamos el namespace

* En cascada se eliminarán:
  * Ingress
  * Deployment
  * Pods
  * PVC y PV por políticas de `Delete`

```bash
kubectl delete ns demo-nextcloud
```

----
## Restauramos lo eliminado

```bash
kubectl create ns demo-nextcloud
## Restauramos completamente el ns
velero create restore --from-backup nextcloud-on-prem \
  --include-namespaces demo-nextcloud
```

----
## Clonar una aplicación
----
## Creamos un nuevo namespace

```bash
kubectl create ns demo-nextcloud-qa
```

----
### Restauramos en el nuevo namespace

```bash
# Preparamos los PVC a partir de los anteriores:
kubectl get pvc -o json \
  | jq 'del(.items[].status,.items[].spec.volumeName,.items[].metadata.namespace,.items[].metadata.annotations)' \
  | kubectl -n demo-nextcloud-qa create -f -

# Restauramos únicamente del backup la aplicación de nextcloud pero en un
# nuevo namespace
velero create restore --from-backup nextcloud-migrate-on-prem \
  --include-namespaces demo-nextcloud \
  --namespace-mappings demo-nextcloud:demo-nextcloud-qa \
  --exclude-resources pv,pvc,ingresses.extensions,ingresses.networking.k8s.io
```
<small class="fragment">Hacemos esto porque restic necesita un PV donde
restaurar los datos. Además, requiere el pod que usa el volumen... <em>Magia
negra</em></small>

----

### Corregimos el nuevo despliegue

Reconfiguramos la aplicación:

```bash
# Cambiamos la configuracion del deploy de nextcloud para que utilice el nuevo dominio                                                                       
kubectl -n demo-nextcloud-qa get cm nextcloud-config -o yaml \
  | sed 's/drive.on-prem.testing.mikroways.net/drive-qa.on-prem.testing.mikroways.net/' \
  | kubectl -n demo-nextcloud-qa apply -f -
kubectl -n demo-nextcloud-qa get deploy -o yaml \
  | sed 's/drive.on-prem.testing.mikroways.net/drive-qa.on-prem.testing.mikroways.net/' \
  | kubectl -n demo-nextcloud-qa apply -f -


# Creamos un nuevo ingress con otro nombre:
kubectl -n demo-nextcloud get ingress nextcloud -o yaml \
  | sed 's/drive.on-prem.testing.mikroways.net/drive-qa.on-prem.testing.mikroways.net/;s/demo-nextcloud/demo-nextcloud-qa/' \
  | kubectl create -f -
```
