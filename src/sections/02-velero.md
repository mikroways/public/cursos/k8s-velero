# Velero

<img class="main" height="150px" src="static/velero.png" />

----
# Velero

* Simplifica la creación de backups y su restauración en un ambiente cloud native.
* Agiliza la migración entre clusters de Kubernetes.
* **No accede** directamente a la base de datos etcd para realizar los backups
sino que utiliza la API de kubernetes e interactúa con recursos del cluster.

----
> Es importante destacar que velero trabaja a nivel recursos de kubernetes
> porque nos permite manejar selectores, namespaces y por ello clasificar y
> selectivamente operar sobre los backups como veremos luego en los ejemplos
----
# Su historia

> Proyecto originalmente desarrollado por Heptio y se llamaba Ark.
> Luego lo adquirió VMWare y actualmente es parte de [Tanzu](https://tanzu.vmware.com/tanzu).

----
# Escenarios

* Backup de recursos y recuperación ante pérdidas.
* Migración de recursos de un cluster a otro cluster.
* Replicación de ambientes en un cluster.

----
# Componentes

* Un servidor que se despliega en el cluster.
* Un cliente que se corre localmente usando un comando.
----
# Modo de uso

* **Por demanda:** `velero create backup`
* **Periódicamente:** `velero create schedule`
* **Retención de backups:** por defecto son 30 días

