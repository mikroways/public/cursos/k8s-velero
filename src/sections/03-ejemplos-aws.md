# Ejemplos en AWS

<img class="main" height="150px" src="static/aws-logo.png" />

----

### En AWS todo es maravilloso

<img class="main" height="550px" src="static/workflow.svg" />

----
## Restauración ante un desastre
----
## Instalamos una aplicación


```bash
cd ejemplos/aws/demo-nextcloud \
  && helmfile sync
```
En el namespace `demo-nextcloud` se instala nextcloud usando helm

<small class="fragment">
Este paso ya lo hicimos para ganar tiempo
</small>
----

## Creamos un backup

```bash
velero create backup nextcloud
```

----

## Verificamos el estado

```bash
velero describe backup nextcloud
# Para ver más detalle
velero describe backup nextcloud --details
```

----

## Eliminamos el namespace

* En cascada se eliminarán:
  * Ingress
  * Deployment
  * Pods
  * PVC y PV por políticas de `Delete`

```bash
kubectl delete ns demo-nextcloud
```

----

## Restauramos lo eliminado

```bash
kubectl create ns demo-nextcloud
## Restauramos completamente el ns
velero create restore --from-backup nextcloud \
  --include-namespaces demo-nextcloud
```

----

## Otra forma de restaurar

En este caso aplicamos IaC

```bash
# Eliminamos y recreamos el namespace
kubectl delete ns demo-nextcloud
kubectl create ns demo-nextcloud

# Restauramos solo pv y pvc
velero create restore --from-backup nextcloud \
  --include-namespaces demo-nextcloud \
  --include-resources pv,pvc

# Reinstalamos con helm
cd ejemplos/aws/demo-nextcloud \
  && helmfile sync \
    --set persistence.existingClaim=nextcloud-nextcloud
```
<small class="fragment">
Observar que se reusa el PVC con helm
</small>
----
## Clonar una aplicación
----
## Creamos un nuevo namespace

```bash
kubectl create ns demo-nextcloud-qa
```

----
### Restauramos en el nuevo namespace

```bash
velero create restore --from-backup nextcloud \
  --include-namespaces demo-nextcloud \
  --namespace-mappings demo-nextcloud:demo-nextcloud-qa \
  --exclude-resources \
    ingresses.extensions,ingresses.networking.k8s.io
```

> Notamos que velero permite mapear el namespace de los objetos de k8s y además
> ignorar algunos recursos. **No recuperamos los ingress.**

----

### Corregimos el nuevo despliegue

Reconfiguramos la aplicación:

```bash
# Cambiamos valores del configmap
kubectl -n demo-nextcloud-qa get cm nextcloud-config -o yaml \
  | sed 's/drive.testing.mikroways.net/drive-qa.testing.mikroways.net/' \
  | kubectl -n demo-nextcloud-qa apply -f -
# Cambiamos valores del despliegue
kubectl -n demo-nextcloud-qa get deploy -o yaml \
  | sed 's/drive.testing.mikroways.net/drive-qa.testing.mikroways.net/' \
  | kubectl -n demo-nextcloud-qa apply -f -
```
----
### Corregimos el nuevo despliegue

Creamos el ingress con nuevo dominio

```bash
kubectl -n demo-nextcloud get ingress nextcloud -o yaml \
  | sed 's/drive.testing.mikroways.net/drive-qa.testing.mikroways.net/;s/demo-nextcloud/demo-nextcloud-qa/' \
  | kubectl create -f -
```

----

### Mismo ejemplo usando helm

```bash
# Creamos un nuevo backup más acotado
velero create backup nextcloud-vols --wait \
  --include-namespaces demo-nextcloud \
  --include-resources pv,pvc 
# Creamos un nuevo namespace
kubectl create ns demo-nextcloud-qa-vols
# Restauramos solo los volúmenes en el nuevo namespace
velero create restore --from-backup nextcloud-vols --wait\
  --namespace-mappings demo-nextcloud:demo-nextcloud-qa-vols
# Instalamos con helm
cd ejemplos/aws/demo-nextcloud-qa-vols && helmfile sync
```


