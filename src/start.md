---
title: Backups de Kubernetes con Velero
theme: white
highlightTheme: vs2015
preprocessor: preproc.js
revealOptions:
  transition: 'slide'
---

# VELERO
## backups de kubernetes

<div class="container">
  <div class="col">
    <img class="main" height="150px" src="static/k8s.png" />
  </div>
  <div class="col">
    <img class="main" height="150px" src="static/velero.png" />
  </div>
</div>

---
# Otras temáticas 

Mirá otros webinars en [nuestro canal de
youtube](https://www.youtube.com/user/Mikroways)
---

FILE: 01-agenda.md

---

FILE: 02-velero.md

---

FILE: 03-ejemplos-aws.md

---

FILE: 04-ejemplos-bare-metal.md

---

FILE: 05-consideraciones.md

---

FILE: 06-conclusiones.md
