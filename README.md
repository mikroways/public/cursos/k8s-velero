# Ejemplos para velero


# En AWS todo es maravilloso

* Primero iniciamos en el namespace `demo-nextcloud` usando helmfile un
  nextcloud: `cd ejemplos/aws/demo-nextcloud && helmfile sync`
  * Accedemos al nextcloud: https://drive.testing.mikroways.net/
  * Creamos una carpeta `Demo velero/`
  * Subimos usando drag and drop los archivos en esta carpeta `files/`

> Para ver los snapshots: `aws ec2 describe-snapshots --owner-id self`

## Recuperación de una aplicación

La idea será eliminar el namespace de la demo de nextcloud y volver a recuperar
la aplicación completa

```
# Creamos un backup
velero create backup nextcloud
## Eliminamos la aplicación completa
kubectl delete ns demo-nextcloud

## Creamos de nuevo el NS
kubectl create ns demo-nextcloud

## Restauramos completamente el ns
velero create restore --from-backup nextcloud \
  --include-namespaces demo-nextcloud

```

### Usando helm y resturando los volumenes

```
kubectl delete ns demo-nextcloud
kubectl create ns demo-nextcloud
velero create restore --from-backup nextcloud-migrate \
  --include-namespaces demo-nextcloud \
  --include-resources pv,pvc

# Reinstalamos con helm
cd ejemplos/aws/demo-nextcloud && helmfile sync --set persistence.existingClaim=nextcloud-nextcloud
```

## Clonamos la aplicación a un nuevo namespace

  ```
  # Creamos nuevo ns
  kubectl create ns demo-nextcloud-qa

  # Backup de nextcloud
  velero create backup nextcloud-migrate

  # Restauramos únicamente del backup la aplicación de nextcloud pero en un
  # nuevo namespace
  velero create restore --from-backup nextcloud \
    --include-namespaces demo-nextcloud \
    --namespace-mappings demo-nextcloud:demo-nextcloud-qa \
    --exclude-resources ingresses.extensions,ingresses.networking.k8s.io

  # Cambiamos la configuracion del deploy de nextcloud para que utilice el nuevo
  # dominio
  kubectl -n demo-nextcloud-qa get cm nextcloud-config -o yaml \
    | sed 's/drive.testing.mikroways.net/drive-qa.testing.mikroways.net/' \
    | kubectl -n demo-nextcloud-qa apply -f -
  kubectl -n demo-nextcloud-qa get deploy -o yaml \
    | sed 's/drive.testing.mikroways.net/drive-qa.testing.mikroways.net/' \
    | kubectl -n demo-nextcloud-qa apply -f -


  # Creamos un nuevo ingress con otro nombre:
  kubectl -n demo-nextcloud get ingress nextcloud -o yaml \
    | sed 's/drive.testing.mikroways.net/drive-qa.testing.mikroways.net/;s/demo-nextcloud/demo-nextcloud-qa/' \
    | kubectl create -f -
  ```

### Usando helm y resturando los volumenes

La idea es usar Infraestructura como código, entonces restauramos los volúmenes
porque el despliegue de la aplicación es igual pero con otra configuración
(Helm/Kustomize hacen mucho mejor la tarea de parametrizar)

  ```
  kubectl create ns demo-nextcloud-qa-vols
  velero create backup nextcloud-vols \
    --include-namespaces demo-nextcloud \
    --include-resources pv,pvc

  # Ahora restauramos primero en el nuevo ns el backup y luego desplegamos
  # usando helm la aplicación nuevamente
  velero create restore --from-backup nextcloud-vols \
    --namespace-mappings demo-nextcloud:demo-nextcloud-qa-vols

  cd ejemplos/aws/demo-nextcloud-qa-vols && helmfile sync
  ```


# Y qué pasa on-prem?  Usamos restic

> Ya está instalado velero con restic on prem

* Primero iniciamos en el namespace `demo-nextcloud` usando helmfile un
  nextcloud: `cd ejemplos/on-prem/demo-nextcloud && helmfile sync`
  * Mostramos que claramente se agregan anotaciones a los PODs que tengan
    volúmenes a backupear
  * Accedemos al nextcloud: https://drive.on-prem.testing.mikroways.net/
  * Creamos una carpeta `Demo velero/`
  * Subimos usando drag and drop los archivos en esta carpeta `files/`

## Recuperación de una aplicación

La idea será eliminar el namespace de la demo de nextcloud y volver a recuperar
la aplicación completa

```
# Backup de nextcloud
velero create backup nextcloud-on-prem \
  --include-namespaces demo-nextcloud
```
> Esperar se realice el backup. Ver con detail el progreso

```
## Eliminamos la aplicación completa
kubectl delete ns demo-nextcloud

## Creamos de nuevo el NS
kubectl create ns demo-nextcloud

## Restauramos completamente el ns
velero create restore --from-backup nextcloud-on-prem \
  --include-namespaces demo-nextcloud

```
## Clonamos la aplicación a un nuevo namespace

```
# Creamos nuevo ns
kubectl create ns demo-nextcloud-qa


# Preparamos los PVC a partir de los anteriores:
kubectl get pvc -o json \
  | jq 'del(.items[].status,.items[].spec.volumeName,.items[].metadata.namespace,.items[].metadata.annotations)' \
  | kubectl -n demo-nextcloud-qa create -f -

# Restauramos únicamente del backup la aplicación de nextcloud pero en un
# nuevo namespace
velero create restore --from-backup nextcloud-migrate-on-prem \
  --include-namespaces demo-nextcloud \
  --namespace-mappings demo-nextcloud:demo-nextcloud-qa \
  --exclude-resources pv,pvc,ingresses.extensions,ingresses.networking.k8s.io

# Cambiamos la configuracion del deploy de nextcloud para que utilice el nuevo
# dominio
kubectl -n demo-nextcloud-qa get cm nextcloud-config -o yaml \
  | sed 's/drive.on-prem.testing.mikroways.net/drive-qa.on-prem.testing.mikroways.net/' \
  | kubectl -n demo-nextcloud-qa apply -f -
kubectl -n demo-nextcloud-qa get deploy -o yaml \
  | sed 's/drive.on-prem.testing.mikroways.net/drive-qa.on-prem.testing.mikroways.net/' \
  | kubectl -n demo-nextcloud-qa apply -f -


# Creamos un nuevo ingress con otro nombre:
kubectl -n demo-nextcloud get ingress nextcloud -o yaml \
  | sed 's/drive.on-prem.testing.mikroways.net/drive-qa.on-prem.testing.mikroways.net/;s/demo-nextcloud/demo-nextcloud-qa/' \
  | kubectl create -f -
```

